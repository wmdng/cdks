/*============================================================================
 * Name        : main.c
 * Author      : $(username)
 * Version     : 0.0.0
 * Copyright   : Your copyright notice
 * Description : Simple function in C, Ansi-style
 *============================================================================
 */

#include <stdio.h>
#include "soc.h"
#include "ck_usart.h"


/**
 * @brief the main entry of the application; when run to here, the system has been initialized includes:
 * 1 CPU processor status register
 * 2 CPU vector base register
 * 3 CPU Units such as MGU, Cache...
 * 4 IO base address
 *
 * @return For MCU application, it's better to loop here
 */
int main()
{
	ck_usart_reg_t * puart = (ck_usart_reg_t *)CSKY_UART_BASE;
	
	/**/
	puart->LCR = LCR_SET_DLAB;
    puart->DLL = 128 & 0xff;
    puart->DLH = 0;
	puart->LCR = LCR_WORD_SIZE_8;
	puart->FCR = 0x3;		/* 1-enable, 2-clear */
	
	/**/
	setvbuf(stdout, NULL, _IONBF, 0);
	
	/**/
	printf( "hello1\n" );
	printf( "hello2\n" );
	printf( "hello3\n" );
    // TODO
    return 0;
}
